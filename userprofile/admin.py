from django.contrib import admin
from .models import UserProfile, Designation, Department, EducationInfo, WorkExperience


class EducationInfoInline(admin.TabularInline):
    model = EducationInfo
    # fieldsets = (
    #     (None, {
    #         'fields': (
    #             ('school_name', 'degree', 'field_of_study', 'completion_date'),
    #         )
    #     }),
    #     ('Extras', {
    #         'fields': (
    #             'additional_notes',
    #         )
    #     }),
    # )


class WorkExperienceInline(admin.TabularInline):
    model = WorkExperience


class UserProfileAdmin(admin.ModelAdmin):
    inlines = [
        EducationInfoInline,
        WorkExperienceInline
    ]

    fieldsets = (
        (None, {
            'fields': (
                ('firstname_en', 'firstname_alt'), ('middlename_en', 'middlename_alt'), ('lastname_en', 'lastname_alt'),
                ('gender', 'email', 'mobile'), ('designation', 'department', 'supervisor')
            )
        }),
        ('Address Information', {
            'fields': ('address_line', ('state', 'city', 'zip_code')),
        }),
    )


class DesignationAdmin(admin.ModelAdmin):
    pass


class DepartmentAdmin(admin.ModelAdmin):
    pass


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Designation, DesignationAdmin)
admin.site.register(Department, DepartmentAdmin)
