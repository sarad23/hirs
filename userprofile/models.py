from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    class Meta:
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profiles'

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('U', 'Unspecified'),
    )

    user = models.OneToOneField(User)
    firstname_en = models.CharField(max_length=200, verbose_name='First Name in English')
    firstname_alt = models.CharField(max_length=200, verbose_name='First Name in Local Language', null=True, blank=True)
    middlename_en = models.CharField(max_length=200, verbose_name='Middle Name in English')
    middlename_alt = models.CharField(max_length=200, verbose_name='Middle Name in Local Language', null=True, blank=True)
    lastname_en = models.CharField(max_length=200, verbose_name='Last Name in English')
    lastname_alt = models.CharField(max_length=200, verbose_name='Last Name in Local Language', null=True, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    email = models.EmailField(max_length=200, verbose_name='Email Address')
    mobile = models.CharField(max_length=15, null=True, blank=True, verbose_name='Mobile Number')
    profile_pic = models.ImageField(upload_to='profile_pics', blank=True, null=True)

    designation = models.ForeignKey('Designation', related_name='user_designation')
    department = models.ForeignKey('Department', related_name='user_department')
    supervisor = models.ForeignKey('self', related_name='user_supervisor')

    address_line = models.TextField(blank=True, null=True, verbose_name='Address Lines')
    state = models.CharField(max_length=100)  # todo: change this to choicefield from values from settings
    city = models.CharField(max_length=150)  # todo: change this to choicefield from values from settings
    zip_code = models.IntegerField(blank=True, null=True)
    joined_date = models.DateTimeField(auto_created=True)

    def __str__(self):
        return '{0} {1} {2} ({3}) [{4}]'.format(self.firstname_en, self.middlename_en, self.lastname_en,
                                                self.designation, self.department)


class Designation(models.Model):
    class Meta:
        verbose_name = 'Designation'
        verbose_name_plural = 'Designations'

    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=150)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return '{0}'.format(self.name)


class Department(models.Model):
    class Meta:
        verbose_name = 'Department'
        verbose_name_plural = 'Departments'

    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=150)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return '{0}'.format(self.name)


class EducationInfo(models.Model):
    class Meta:
        verbose_name = 'Education Information'
        verbose_name_plural = 'Education Information'

    DEGREE_CHOICES = (
        ('HS', 'High School'),
        ('IN', 'Intermediate'),
        ('DI', 'Diploma'),
        ('BS', 'Bachelors'),
        ('MS', 'Masters'),
        ('PH', 'Doctorate'),
        ('PD', 'Post Doctorate'),
    )

    user_profile = models.ForeignKey('UserProfile', related_name='user_education_info')
    school_name = models.CharField(max_length=200, null=True, blank=True)
    degree = models.CharField(max_length=2, choices=DEGREE_CHOICES, null=True, blank=True)
    field_of_study = models.CharField(max_length=200, null=True, blank=True)
    completion_date = models.DateField(null=True, blank=True)
    additional_notes = models.TextField(null=True, blank=True)


class WorkExperience(models.Model):
    class Meta:
        verbose_name = 'Work Experience'
        verbose_name_plural = 'Work Experiences'

    user_profile = models.ForeignKey('UserProfile', related_name='user_work_info')
    company_name = models.CharField(max_length=200, null=True, blank=True)
    job_title = models.CharField(max_length=200, null=True, blank=True)
    from_date = models.DateField(null=True, blank=True)
    to_date = models.DateField(null=True, blank=True)
    job_description = models.TextField(null=True, blank=True)

