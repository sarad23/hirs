from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required


def list_employee(request):
    alert_level = "success"
    message_tag = "Success!"
    created_information = ""
    extra_message = ""
    if request.GET.get('a'):
        message_tag = "Success!"
        created_information = request.GET.get('a')
        extra_message = "has been created successfully."

    if request.GET.get('u'):
        message_tag = "Success!"
        created_information = request.GET.get('u')
        extra_message = "information was updated successfully."

    demo_data = [
        {
            'photo': 'assets/img/neiha-joshi.jpg',
            'name': 'Neiha Joshi',
            'email': 'neiha.joshi@rosebayconsult.com',
            'status': 'Active',
            'class': 'success',
            'dep': 'Sales and Marketing',
            'd': 'Manager - Commercial Alliances',
            'j': '2 June,2016'
        },
        {
            'photo': 'assets/img/sabnam-tamrakar.jpg',
            'name': 'Sabnam Tamrakar',
            'email': 'sabnam@rosebayconsult.com',
            'status': 'Inactive',
            'class': 'danger',
            'dep': 'Sales and Marketing',
            'd': 'Manager',
            'j': '14 August,2016'
        },
        {
            'photo': 'assets/img/shristi-shakya.jpg',
            'name': 'Shristi Shakya',
            'email': 'shristi@rosebayconsult.com',
            'status': 'Pending',
            'class': 'warning',
            'dep': 'Sales and Marketing',
            'd': 'Marketing Associate',
            'j': '23 August,2016'
        },
        {
            'photo': 'assets/img/udaya-bajracharya.jpg',
            'name': 'Udaya Bajracharya',
            'email': 'udaya@rosebayconsult.com',
            'status': 'Active',
            'class': 'success',
            'dep': 'Sales and Marketing',
            'd': 'Relationship Manager',
            'j': '14 Septemebr,2016'
        }
    ]

    data = {
        'alert_level': alert_level,
        'message_tag': message_tag,
        'created_information': created_information,
        'extra_message': extra_message,
        'demo_data': demo_data,
        'activePage': {'tree': 'OrganizationPage', 'branch': 'employee_list'},
    }
    return render(request, 'organization/employee-list.html', data)


def create_employee(request):
    data = {
        'activePage': {'tree': 'OrganizationPage', 'branch': 'employee_create'},
    }
    return render(request, 'organization/employee-create.html', data)


def list_department(request):
    alert_level = "success"
    message_tag = "Success!"
    created_information = ""
    extra_message = ""
    if request.GET.get('a'):
        message_tag = "Success!"
        created_information = request.GET.get('a')
        extra_message = "has been created successfully."

    if request.GET.get('u'):
        message_tag = "Success!"
        created_information = request.GET.get('u')
        extra_message = "information was updated successfully."

    data = {
        'alert_level': alert_level,
        'message_tag': message_tag,
        'created_information': created_information,
        'extra_message': extra_message,
        'activePage': {'tree': 'OrganizationPage', 'branch': 'department_list'},
    }
    return render(request, 'organization/department-list.html', data)


def list_designation(request):
    alert_level = "success"
    message_tag = "Success!"
    created_information = ""
    extra_message = ""
    if request.GET.get('a'):
        message_tag = "Success!"
        created_information = request.GET.get('a')
        extra_message = "has been created successfully."

    if request.GET.get('u'):
        message_tag = "Success!"
        created_information = request.GET.get('u')
        extra_message = "information was updated successfully."

    data = {
        'alert_level': alert_level,
        'message_tag': message_tag,
        'created_information': created_information,
        'extra_message': extra_message,
        'activePage': {'tree': 'OrganizationPage', 'branch': 'department_list'},
    }
    return render(request, 'organization/designation-list.html', data)
