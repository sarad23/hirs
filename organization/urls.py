from django.conf.urls import url
from .views import list_employee, create_employee, list_department, list_designation

from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^employee-list/$', list_employee, name='employee_list'),
    url(r'^employee-create/$', create_employee, name='employee_create'),

    url(r'^department-list/$', list_department, name='department_list'),

    url(r'^designation-list/$', list_designation, name='designation_list'),
]
