from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from .forms import LoginForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.messages import get_messages
from rest_framework.decorators import api_view
from rest_framework.response import Response


def http_response(status, message):
    """
    wrapper class to send Response back
    :param status: boolean (True or False)
    :param message: string (Success or Error message)
    :return: Response() object
    """
    # todo: do we need a content-type of application/json here or does DRF handle it???
    return Response({'status': status, 'message': message})


def userlogin(request):
    message = ''
    data = {
        'message': message
    }
    return render(request, 'login.html', data)


@api_view(['POST'])
def login_validation(request):
    message = 'Something is wrong.'
    if request.method == 'POST':
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)

        if not (username or password):
            return http_response(False, 'Either Username or Password not supplied.')

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return http_response(True, 'Login Successful')
            else:
                return http_response(False, 'User Account not active.')

    return http_response(False, message)


def userlogout(request):
    logout(request)
    return redirect(reverse('login'))
