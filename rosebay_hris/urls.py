"""rosebay_hris URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import *
from django.contrib import admin
from .views import userlogin, login_validation, userlogout
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^', include("dashboard.urls", namespace="dashboard")),
    url(r'^organization/', include("organization.urls", namespace="organization")),
    url(r'^attendance/', include("attendance.urls", namespace="attendance")),
    url(r'^leave-management/', include("leave_management.urls", namespace="leave_management")),
    url(r'^payroll-management/', include("payroll_management.urls", namespace="payroll_management")),

    url(r'^admin/', admin.site.urls),
    url(r'^login/', userlogin, name='login'),
    url(r'^login-validation/', login_validation, name='login_validation'),
    url(r'^logout/', userlogout, name="logout"),
    # url(r'^login/$', auth_views.login, {'template_name': 'login.html'}),
]
