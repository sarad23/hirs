from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required


def list_payroll(request):
    alert_level = "success"
    message_tag = "Success!"
    created_information = ""
    extra_message = ""
    if request.GET.get('a'):
        message_tag = "Success!"
        created_information = "Salary Sheet"
        extra_message = "was created successfully."

    if request.GET.get('u'):
        message_tag = "Success!"
        created_information = "Salary Sheet"
        extra_message = "was uploaded successfully."

    demo_data = [
        {
            'photo': 'assets/img/neiha-joshi.jpg',
            'name': 'Neiha Joshi',
            'email': 'neiha.joshi@rosebayconsult.com',
            'status': 'Active',
            'class': 'success',
            'dep': 'Sales and Marketing',
            'd': 'Manager - Commercial Alliances',
            'j': '2 June,2016',
            'g': "Female"
        },
        {
            'photo': 'assets/img/sabnam-tamrakar.jpg',
            'name': 'Sabnam Tamrakar',
            'email': 'sabnam@rosebayconsult.com',
            'status': 'Inactive',
            'class': 'danger',
            'dep': 'Sales and Marketing',
            'd': 'Manager',
            'j': '14 August,2016',
            'g': "Female"
        },
        {
            'photo': 'assets/img/shristi-shakya.jpg',
            'name': 'Shristi Shakya',
            'email': 'shristi@rosebayconsult.com',
            'status': 'Pending',
            'class': 'warning',
            'dep': 'Sales and Marketing',
            'd': 'Marketing Associate',
            'j': '23 August,2016',
            'g': "Female"
        },
        {
            'photo': 'assets/img/udaya-bajracharya.jpg',
            'name': 'Udaya Bajracharya',
            'email': 'udaya@rosebayconsult.com',
            'status': 'Active',
            'class': 'success',
            'dep': 'Sales and Marketing',
            'd': 'Relationship Manager',
            'j': '14 Septemebr,2016',
            'g': "Male"
        }
    ]

    months = ["Janaury", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    data = {
        'alert_level': alert_level,
        'message_tag': message_tag,
        'created_information': created_information,
        'extra_message': extra_message,
        'demo_data': demo_data,
        'months': months,
        'activePage': {'tree': 'PayrollPage', 'branch': 'payroll_list'},
    }
    return render(request, 'payroll_management/payroll-list.html', data)


def upload_salary_sheet(request):
    data = {
        'activePage': {'tree': 'PayrollPage', 'branch': 'upload_salary_sheet'},
    }
    return render(request, 'payroll_management/upload-salary-sheet.html', data)


def payroll_setting(request):
    alert_level = "success"
    message_tag = "Success!"
    created_information = ""
    extra_message = ""
    if request.GET.get('u'):
        message_tag = "Success!"
        created_information = "Payroll Setting"
        extra_message = "was updated successfully."

    data = {
        'alert_level': alert_level,
        'message_tag': message_tag,
        'created_information': created_information,
        'extra_message': extra_message,
        'activePage': {'tree': 'PayrollPage', 'branch': 'payroll_setting'},
    }
    return render(request, 'payroll_management/payroll-setting.html', data)


def tax_amount_setting(request):
    data = {
        'activePage': {'tree': 'PayrollPage', 'branch': 'tax_amount_setting'},
    }
    return render(request, 'payroll_management/tax-amount-setting.html', data)
