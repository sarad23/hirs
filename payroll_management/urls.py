from django.conf.urls import url
from .views import list_payroll, upload_salary_sheet, payroll_setting, tax_amount_setting

from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^payroll-list/$', list_payroll, name='payroll_list'),
    url(r'^upload-salary-sheet/$', upload_salary_sheet, name='upload_salary_sheet'),

    url(r'^payroll-setting/$', payroll_setting, name='payroll_setting'),

    url(r'^tax-amount-setting/$', tax_amount_setting, name='tax_amount_setting'),
]
