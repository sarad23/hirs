from __future__ import unicode_literals

from datetime import datetime
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from django.conf import settings
from django.contrib.auth.models import Group
from django.db import models


class UserRole(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user_roles')
    group = models.ForeignKey(Group)
    started_at = models.DateTimeField(default=datetime.now)
    ended_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return 'user: {}\'s role : {}'.format(self.user.__unicode__(), self.group.__unicode__())

    class Meta:
        unique_together = ('user', 'group')

    @staticmethod
    def is_active(user, group):
        return UserRole.objects.filter(user=user, group__name=group, ended_at=None).exists()

    @staticmethod
    def get_active_roles(user):
        return UserRole.objects.filter(user=user, ended_at=None).select_related('group')
