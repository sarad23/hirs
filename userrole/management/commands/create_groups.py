from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group
import json


class Command(BaseCommand):
    help = 'Create default groups'

    def handle(self, *args, **options):
        group_list = [{'id': 1, 'name': 'Super Admin'}]
        for group in group_list:
            new_group, created = Group.objects.get_or_create(id=group['id'], name=group['name'])
            if created:
                self.stdout.write('Successfully created group .. "%s"' % json.dumps(group))
