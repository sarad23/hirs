from functools import wraps

from django.views.generic.edit import UpdateView as BaseUpdateView, CreateView as BaseCreateView, DeleteView as \
    BaseDeleteView
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied


class UpdateView(BaseUpdateView):
    def get_context_data(self, **kwargs):
        context = super(UpdateView, self).get_context_data(**kwargs)
        context['scenario'] = _('Edit')
        super(UpdateView, self).get_context_data()
        return context

    def post(self, request, *args, **kwargs):
        response = super(UpdateView, self).post(request, *args, **kwargs)
        if hasattr(response, 'context_data') and not response.context_data.get('form').is_valid():
            messages.warning(request, ('%s %s' % (self.object.__class__._meta.verbose_name.title(),
                                                  _('Update Data Not Valid!'))))
        else:

            messages.success(request, ('%s %s' % (self.object.__class__._meta.verbose_name.title(),
                                                  _('successfully Updated!'))))
        return response


class DeleteView(BaseDeleteView):
    def get(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        response = super(DeleteView, self).post(request, *args, **kwargs)
        messages.success(request, ('%s %s' % (self.object.__class__._meta.verbose_name.title(),
                                              _('successfully deleted!'))))
        return response


class CreateView(BaseCreateView):
    def get_context_data(self, **kwargs):
        context = super(CreateView, self).get_context_data(**kwargs)
        context['scenario'] = _('Add')
        return context

    def post(self, request, *args, **kwargs):
        response = super(CreateView, self).post(request, *args, **kwargs)
        if self.object:
            messages.success(request, ('%s %s' % (self.object.__class__._meta.verbose_name.title(),
                                                  _('successfully created!'))))
        return response


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **kwargs):
        view = super(LoginRequiredMixin, cls).as_view(**kwargs)
        return login_required(view)


USURPERS = {
    'rose_admin': ['Super Admin'],
}


class SuperAdminMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            if request.role.group.name in USURPERS['rose_admin']:
                return super(SuperAdminMixin, self).dispatch(request, *args, **kwargs)
        raise PermissionDenied()


def group_required(group_name):
    def _check_group(view_func):
        @wraps(view_func)
        def wrapper(request, *args, **kwargs):
            if request.user.is_authenticated():
                if request.role.group.name in USURPERS.get(group_name, []):
                    return view_func(request, *args, **kwargs)
            raise PermissionDenied()

        return wrapper

    return _check_group
