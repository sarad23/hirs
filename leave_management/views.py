from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required


def list_leave(request):
    alert_level = "success"
    message_tag = "Success!"
    created_information = ""
    extra_message = ""
    if request.GET.get('a'):
        message_tag = "Success!"
        created_information = request.GET.get('a')
        extra_message = "has been created successfully."

    if request.GET.get('u'):
        message_tag = "Success!"
        created_information = request.GET.get('u')
        extra_message = "information was updated successfully."

    data = {
        'alert_level': alert_level,
        'message_tag': message_tag,
        'created_information': created_information,
        'extra_message': extra_message,
        'activePage': {'tree': 'LeavePage', 'branch': 'leave_list'},
    }
    return render(request, 'leave_management/leave-list.html', data)


def list_applied_leave(request):
    alert_level = "success"
    message_tag = "Success!"
    created_information = ""
    extra_message = ""
    if request.GET.get('u'):
        message_tag = "Success!"
        created_information = request.GET.get('u')
        extra_message = "information was updated successfully."

    data = {
        'alert_level': alert_level,
        'message_tag': message_tag,
        'created_information': created_information,
        'extra_message': extra_message,
        'activePage': {'tree': 'LeavePage', 'branch': 'applied_leave'},
    }
    return render(request, 'leave_management/applied-leave-list.html', data)


def leave_report(request):

    data = {
        'activePage': {'tree': 'LeavePage', 'branch': 'leave_report'},
    }
    return render(request, 'leave_management/leave-report.html', data)
