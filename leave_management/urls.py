from django.conf.urls import url
from .views import list_leave, list_applied_leave, leave_report

from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^leave-list/$', list_leave, name='leave_list'),

    url(r'^applied-leave-list/$', list_applied_leave, name='applied_leave_list'),

    url(r'^leave-report/$', leave_report, name='leave_report'),
]
