from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required


@login_required
def index(request):
    data = {
        'activePage': {'tree': 'DashboardPage', 'branch': 'index'},
    }
    return render(request, 'dashboard/index.html', data)
