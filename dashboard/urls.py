from django.conf.urls import url
from .views import index

from django.contrib.auth.decorators import login_required

urlpatterns = [
    # ex: /polls/
    url(r'^$', index, name='index'),

]
