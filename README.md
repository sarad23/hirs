Rosebay HRIS
-------------------

Developer Setup
====================
1. Clone the repo ```git clone ssh://git-codecommit.us-east-2.amazonaws.com/v1/repos/rosebay-hris```
2. Create your virtual environment and activate it ```virtualenv env; source env/bin/activate```
3. Install the dependencies ```pip install -r requirements.txt```
4. Create your local settings file and customize if needed ```cp rosebay_hris/local_settings.py.SAMPLE rosebay_hris/local_settings.py```
5. If you customized the file to use local db other than the provided sqlite, run the migration first (Optional, depending upon step 4)
    To run the migration: ```python manage.py migrate```
6. Run the dev server and enjoy. ```python manage.py runserver```
