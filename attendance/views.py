from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required


def view_attendance(request):
    alert_level = "success"
    message_tag = "Success!"
    created_information = ""
    extra_message = ""
    if request.GET.get('u'):
        message_tag = "Success!"
        created_information = "Attendance"
        extra_message = "was uploaded successfully."

    data = {
        'alert_level': alert_level,
        'message_tag': message_tag,
        'created_information': created_information,
        'extra_message': extra_message,
        'activePage': {'tree': 'AttendancePage', 'branch': 'attendance_view'},
    }
    return render(request, 'attendance/attendance-list.html', data)


def view_event_calender(request):
    data = {
        'activePage': {'tree': 'AttendancePage', 'branch': 'event_calender'},
    }
    return render(request, 'attendance/view-event-calender.html', data)


def view_overtime(request):
    demo_data = [
        {
            'photo': 'assets/img/neiha-joshi.jpg',
            'name': 'Neiha Joshi',
            'email': 'neiha.joshi@rosebayconsult.com',
            'status': 'Active',
            'class': 'success',
            'dep': 'Sales and Marketing',
            'd': 'Manager - Commercial Alliances',
            'j': '2 June,2016',
            'g': "Female"
        },
        {
            'photo': 'assets/img/sabnam-tamrakar.jpg',
            'name': 'Sabnam Tamrakar',
            'email': 'sabnam@rosebayconsult.com',
            'status': 'Inactive',
            'class': 'danger',
            'dep': 'Sales and Marketing',
            'd': 'Manager',
            'j': '14 August,2016',
            'g': "Female"
        },
        {
            'photo': 'assets/img/shristi-shakya.jpg',
            'name': 'Shristi Shakya',
            'email': 'shristi@rosebayconsult.com',
            'status': 'Pending',
            'class': 'warning',
            'dep': 'Sales and Marketing',
            'd': 'Marketing Associate',
            'j': '23 August,2016',
            'g': "Female"
        },
        {
            'photo': 'assets/img/udaya-bajracharya.jpg',
            'name': 'Udaya Bajracharya',
            'email': 'udaya@rosebayconsult.com',
            'status': 'Active',
            'class': 'success',
            'dep': 'Sales and Marketing',
            'd': 'Relationship Manager',
            'j': '14 Septemebr,2016',
            'g': "Male"
        }
    ]
    data = {
        'demo_data': demo_data,
        'activePage': {'tree': 'AttendancePage', 'branch': 'overtime'},
    }
    return render(request, 'attendance/overtime-view.html', data)
