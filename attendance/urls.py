from django.conf.urls import url
from .views import view_attendance, view_event_calender, view_overtime

from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^attendance-view/$', view_attendance, name='attendance_view'),

    url(r'^event-calender-view/$', view_event_calender, name='event_calender_view'),

    url(r'^overtime-view/$', view_overtime, name='overtime_view'),
]
